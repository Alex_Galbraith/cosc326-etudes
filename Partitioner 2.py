import math
import time
from heapq import heappush, heappop
from random import uniform
from tkinter import *

NUM_POINTS = 11
points = []
r1 = None;

drawStack = []

debugCount = 0;

def make_points():
    global points
    fnf = True
    while fnf:
        #filename = input('Enter either "[filename].txt" or "[number of telephone sites] [random float range min] [random float range max]": ')
        #filename = "telephone_sites.txt"
        #filename = "5000 0 2000"
        filename = "1000 20 620"

        try:
            sep1_index = filename.index(' ')
            num_sites = int(filename[0 : sep1_index])
            sep2_index = filename.index(' ', sep1_index + 1)
            min_range = int(filename[sep1_index + 1 : sep2_index])
            max_range = int(filename[sep2_index + 1 : ])

            for i in range(num_sites):
                x = uniform(min_range, max_range)
                y = uniform(min_range, max_range)
                points += [(x, y)]
                fnf = False

        except ValueError:
            try:
                with open(filename, 'r') as f:
                    for line in f:
                        try:
                            sep = line.index(' ')
                            x = float(line[0 : sep])
                            y = float(line[sep + 1 : ])
                            points += [(x, y)]
                        except ValueError:
                            continue #Skips the first line or any other invalids
                fnf = False
            except FileNotFoundError:
                print("Error: '{}' could not be found!".format(filename))


def modifyXY(xy, d):
    return (xy[0] + d, xy[1] + d)

canvas = None
def show():
    global canvas
    id = 0
    global min_radius
    min_radius = 1000
    global circle_id, text_id, canvas, min_circle
    window = Tk()
    window.title("Cordless Phones")
    canvas = Canvas(window, width=640, height=640)
    canvas.pack()
    text_id = canvas.create_text(580, 620, width=100, text="")
    circle_id = canvas.create_oval(modifyXY(points[id], float('inf')), modifyXY(points[id], float('inf')),
                                   outline="black")
    min_circle = canvas.create_oval(modifyXY(points[id], float('inf')), modifyXY(points[id], float('inf')),
                                    outline="red")

    for i in range(len(points)):
        canvas.create_oval(modifyXY(points[i], -1), modifyXY(points[i], 1), fill="black")



    def moveCircle(event):
        searchpart(-1)
        print((resolved_min))
        print(min_pt)
        r1.draw(canvas)
        while (len(drawStack) > 0):
            drawStack.pop(0).draw(canvas)
        canvas.create_oval(modifyXY(min_region.minpt, -1), modifyXY(min_region.minpt, 1), fill="red", outline="blue")
        canvas.create_oval(modifyXY(min_pt, -1), modifyXY(min_pt, 1), fill="red", outline="red")

        global circle_id
        global min_radius, min_circle
        canvas.delete(circle_id)
        mouse = (window.winfo_pointerx() - window.winfo_rootx(), window.winfo_pointery() - window.winfo_rooty())
        dists = []
        for p in points:
            dx = mouse[0] - p[0]
            dy = mouse[1] - p[1]
            dxy = dx ** 2 + dy ** 2
            dists.append(dxy)
        dists.sort()
        radius = math.sqrt(dists[NUM_POINTS])
        if radius < min_radius:
            min_radius = radius
            canvas.delete(min_circle)
            min_circle = canvas.create_oval(modifyXY(mouse, -radius), modifyXY(mouse, radius), outline="red")
        canvas.itemconfig(text_id, text=str(int(radius)) + ', min: ' + str(int(min_radius)) + " mx,y: "+str(mouse[0]) +" "+ str(mouse[1]))
        circle_id = canvas.create_oval(modifyXY(mouse, -radius), modifyXY(mouse, radius), outline="black")

    window.bind('<Motion>', moveCircle)
    window.mainloop()


def evaluate_at_point(point, pts = points, n = NUM_POINTS):
    dists = []

    for p in pts:
        dists.append(dist(point,p))

    dists.sort()
    radius = dists[n]
    return radius


def dist_sq(a,b):
    dx = a[0] - b[0]
    dy = a[1] - b[1]
    dxy = dx ** 2 + dy ** 2
    return dxy

def dist(a,b):
    return math.sqrt(dist_sq(a,b))


heap = []
resolved_min = float('inf')
min_pt = [0,0]
max_precision = 10
min_region = None

def mult_arr(arr, scalar):
    out = []
    for i in range(len(arr)):
        out.append(arr[i] * scalar)
    return out

def add_arr(a,b):
    if(len(a) != len(b)):
        if(len (a)>len(b)):
            (a,b) = (b,a)
    out = []
    for i in range(len(a)):
        out.append(a[i] + b[i])
    return out

class Region:
    min = [0,0]
    max = [0,0]
    minvalue = float('inf')
    minpt = [0,0]
    '''in clockwise order from top left'''
    corner_vals = [0,0,0,0]
    corners = [[0,0],[0,0],[0,0],[0,0]]
    subregions = [None,None,None,None]


    def __init__(self, min, max):
        self.min = min
        self.max = max
        subregions = [None, None, None, None]

        #print("MAX: " + str(self.max))


    def calc_corners(self):
        global resolved_min
        global min_region
        global min_pt
        self.corners[0] = self.min
        self.corners[1] = [self.max[0], self.min[1]]
        self.corners[2] = self.max
        self.corners[3] = [self.min[0], self.max[1]]
        self.corner_vals[0] = evaluate_at_point(self.min)
        self.corner_vals[1] = evaluate_at_point(self.corners[1])
        self.corner_vals[2] = evaluate_at_point(self.max)
        self.corner_vals[3] = evaluate_at_point(self.corners[3])

        for i in range(len(self.corner_vals)):
            if self.corner_vals[i]<resolved_min:
                resolved_min = self.corner_vals[i]
                min_pt = self.corners[i]
                min_region = self

    def get_precision(self):
        #return dist(self.min,self.max)
        return min(self.max[0]-self.min[0],self.max[1]-self.min[1])

    def calc_min(self):
        self.calc_corners()
        point = [0,0]
        #print(self.corners[1])
        for i in range(4):
            f = self.corner_vals[i]
            point = add_arr(point, mult_arr(self.corners[i], f))
            #print(self.corners[i])
            #print(point)
            #print('---')
        sum = self.corner_vals[0] + self.corner_vals[1] + self.corner_vals[2] + self.corner_vals[3]
        point = [point[0]/sum,point[1]/sum]
        sum/=4

        self.minpt = point

        self.minvalue = self.corner_vals[0] - dist(self.min,self.minpt)

        return (point,sum)


    def subdivide(self):

        global debugCount
        if self.get_precision() < max_precision:
            debugCount += 1
            if(debugCount%100 == 0):
                print("REACHED DEEPEST "+str(debugCount))
            return
        #print("SUBDIVIEDED")
        #print(self.subregions)
        #print('----')




        mid_top = [self.minpt[0],self.min[1]]
        mid_bot = [self.minpt[0],self.max[1]]
        mid_left = [self.min[0],self.minpt[1]]
        mid_right = [self.max[0], self.minpt[1]]
        mid = self.minpt;

        self.subregions[0] = Region(self.min,mid)
        self.subregions[1] = Region(mid_top, mid_right)
        self.subregions[2] = Region(mid, self.max)
        self.subregions[3] = Region(mid_left, mid_bot)
        '''
        mid_top = [(self.min[0] + self.max[0]) / 2, self.min[1]]
        mid_bot = [(self.min[0] + self.max[0]) / 2, self.max[1]]
        mid_left = [self.min[0], (self.min[1] + self.max[1]) / 2]
        mid_right = [self.max[0], (self.min[1] + self.max[1]) / 2]
        mid = mult_arr(add_arr(self.min, self.max), 0.5)
        self.subregions[0] = Region(self.min, mid)
        self.subregions[1] = Region(mid_top, mid_right)
        self.subregions[2] = Region(mid, self.max)
        self.subregions[3] = Region(mid_left, mid_bot)'''


        for r in self.subregions:
            r.calc_min()
            drawStack.append(r)
            r.subregions = [None,None,None,None]
            if (r == self):
                print("WTF")
                return

        for r in self.subregions:
            if (abs(r.minvalue - self.minvalue) < 0.01):
                continue;
            if r.minvalue > resolved_min:
                #print("Minval: " + str(r.minvalue))
                #print("Resolved: " + str(resolved_min))
                continue

            heappush(heap,(r.minvalue,r))

    def draw(self, canvas):
        #print(self.min)
        canvas.create_rectangle(self.min[0], self.min[1], self.max[0], self.max[1])
        for r in self.subregions:
            if (r != None):
                if (r == self):
                    print("WHAT WHY")
                    return;





def search():
    global r1
    minx = points[0][0]
    miny = points[0][1]
    maxx = minx
    maxy = miny

    for p in points:
        if p[0] < minx:
            minx = p[0]
        if p[0] > maxx:
            maxx = p[0]
        if p[1] < miny:
            miny = p[1]
        if p[1] > maxy:
            maxy = p[1]

    r1 = Region([minx,miny],[maxx,maxy])
    r1.calc_min()
    heappush(heap,(r1.minvalue,r1))

        #print(resolved_min)
        #print(min_pt)
        #print(len(heap))


def searchpart(n):
    count = 0
    print(heap)
    while (len(heap) > 0 and (count < n or n<0)):
        r = heappop(heap)[1]
        r.calc_min()
        r.subdivide()
        count += 1

make_points()

search()

print((resolved_min))
print(min_pt)
show()
