import scipy.spatial as spatial
import matplotlib.pyplot as plt
import numpy as np
from random import uniform
import smallest_circle as sc
import time




points = None
smallest_circle = None
smallest_diameter = float('inf')
enclose = 12



def plot(points, solution, circles = None):
    print(smallest_circle)
    fig, ax = plt.subplots(dpi = 300)
    plt.scatter(points[:,0], points[:,1],  s = 0.1)
    
    plt.axis('equal')
    c = plt.Circle(solution[:2], solution[2], color='red', fill=False)
    ax.add_artist(c)
    
    for cc in circles:
        if cc[2] < 1.2 * solution[2]:
            c = plt.Circle(cc[:2], cc[2], color='m', fill=False, alpha = 0.2, lw = 0.4)
            ax.add_artist(c)
    
    plt.show()



def make_points(n, min_range, max_range):
    global points;
    points = np.empty([n,2])
    for i in range(n):
        x = round(uniform(min_range, max_range))
        y = round(uniform(min_range, max_range))
        points[i] = np.array([x, y])


make_points(100000,0,6000)



tree = spatial.KDTree(points)
circles = []

start = time.time()
for p in points:
    g = tree.query([p], enclose, distance_upper_bound = smallest_diameter)[1][0]
    if(g[len(g)-1] == len(points)):
        continue
    subset = []
    for i in g:
        subset.append(points[i])
    D = sc.make_circle(subset)
    circles.append(D)
    if (D[2] < smallest_diameter):
        smallest_circle = D
        smallest_diameter = D[2]
end = time.time()
print(end - start)

        

plot(points, smallest_circle, circles)
print(smallest_circle)
    

