import math
import time
from heapq import heappush, heappop
from random import uniform
from random import random
from tkinter import *

NUM_POINTS = 11
points = []


debugCount = 0;

def make_points():
    global points
    fnf = True
    while fnf:
        #filename = input('Enter either "[filename].txt" or "[number of telephone sites] [random float range min] [random float range max]": ')
        #filename = "telephone_sites.txt"
        #filename = "5000 0 2000"
        filename = "50 20 620"

        try:
            sep1_index = filename.index(' ')
            num_sites = int(filename[0 : sep1_index])
            sep2_index = filename.index(' ', sep1_index + 1)
            min_range = int(filename[sep1_index + 1 : sep2_index])
            max_range = int(filename[sep2_index + 1 : ])

            for i in range(num_sites):
                x = uniform(min_range, max_range)
                y = uniform(min_range, max_range)
                points += [(x, y)]
                fnf = False

        except ValueError:
            try:
                with open(filename, 'r') as f:
                    for line in f:
                        try:
                            sep = line.index(' ')
                            x = float(line[0 : sep])
                            y = float(line[sep + 1 : ])
                            points += [(x, y)]
                        except ValueError:
                            continue #Skips the first line or any other invalids
                fnf = False
            except FileNotFoundError:
                print("Error: '{}' could not be found!".format(filename))


def modifyXY(xy, d):
    return (xy[0] + d, xy[1] + d)

canvas = None
def show():
    global canvas
    id = 0
    global min_radius
    min_radius = 1000
    global circle_id, text_id, canvas, min_circle
    window = Tk()
    window.title("Cordless Phones")
    canvas = Canvas(window, width=640, height=640)
    canvas.pack()
    text_id = canvas.create_text(580, 620, width=100, text="")
    circle_id = canvas.create_oval(modifyXY(points[id], float('inf')), modifyXY(points[id], float('inf')),
                                   outline="black")
    min_circle = canvas.create_oval(modifyXY(points[id], float('inf')), modifyXY(points[id], float('inf')),
                                    outline="red")

    for i in range(len(points)):
        canvas.create_oval(modifyXY(points[i], -1), modifyXY(points[i], 1), fill="black")

    canvas.create_oval(modifyXY(min_region.minpt, -1), modifyXY(min_region.minpt, 1), fill="red", outline="red")

    def moveCircle(event):
        global circle_id
        global min_radius, min_circle
        canvas.delete(circle_id)
        mouse = (window.winfo_pointerx() - window.winfo_rootx(), window.winfo_pointery() - window.winfo_rooty())
        dists = []
        for p in points:
            dx = mouse[0] - p[0]
            dy = mouse[1] - p[1]
            dxy = dx ** 2 + dy ** 2
            dists.append(dxy)
        dists.sort()
        radius = math.sqrt(dists[NUM_POINTS])
        if radius < min_radius:
            min_radius = radius
            canvas.delete(min_circle)
            min_circle = canvas.create_oval(modifyXY(mouse, -radius), modifyXY(mouse, radius), outline="red")
        canvas.itemconfig(text_id, text=str(int(radius)) + ', min: ' + str(int(min_radius)) + " mx,y: "+str(mouse[0]) +" "+ str(mouse[1]))
        circle_id = canvas.create_oval(modifyXY(mouse, -radius), modifyXY(mouse, radius), outline="black")

    window.bind('<Motion>', moveCircle)
    window.mainloop()


def evaluate_at_point(point, pts = points, n = NUM_POINTS):
    dists = []

    for p in pts:
        dists.append(dist(point,p))

    dists.sort()
    radius = dists[n]
    return radius


def dist_sq(a,b):
    dx = a[0] - b[0]
    dy = a[1] - b[1]
    dxy = dx ** 2 + dy ** 2
    return dxy

def dist(a,b):
    return math.sqrt(dist_sq(a,b))


heap = []
resolved_min = float('inf')
min_pt = [0,0]
max_precision = 10
min_region = None

def mult_arr(arr, scalar):
    out = []
    for i in range(len(arr)):
        out.append(arr[i] * scalar)
    return out

def add_arr(a,b):
    if(len(a) != len(b)):
        if(len (a)>len(b)):
            (a,b) = (b,a)
    out = []
    for i in range(len(a)):
        out.append(a[i] + b[i])
    return out



class Region:
    minvalue = float('inf')
    minpt = [0,0]
    '''in clockwise order from top left'''
    corner_vals = [0,0,0]
    corners = [[0,0],[0,0],[0,0]]
    subregions = [None,None,None]


    def __init__(self, corners):
        self.corners = corners

        #print("MAX: " + str(self.max))


    def calc_corners(self):
        global resolved_min
        global min_region
        global min_pt
        self.corner_vals[0] = evaluate_at_point(self.corners[0])

        self.corner_vals[1] = evaluate_at_point(self.corners[1])

        self.corner_vals[2] = evaluate_at_point(self.corners[2])


        #Check if we are the optimal region and set
        for i in range(len(self.corner_vals)):
            if self.corner_vals[i]<resolved_min:
                resolved_min = self.corner_vals[i]
                min_pt = self.corners[i]
                min_region = self


    def get_precision(self):
        return max(
            dist(self.corners[0],self.corners[1]),
            dist(self.corners[1], self.corners[2]),
            dist(self.corners[0], self.corners[2]),
        )

    def calc_min(self):
        self.calc_corners()
        point = [0,0]

        for i in range(3):
            f = self.corner_vals[i]
            point = add_arr(point,mult_arr(self.corners[i],f))

        msum = self.corner_vals[0] + self.corner_vals[1] + self.corner_vals[2]
        point = [point[0]/msum,point[1]/msum]
        msum/=3

        self.minpt = point
        self.minvalue = self.corner_vals[0] - dist(self.corners[0],self.minpt)

        print("Cval 0: " + str(self.corner_vals[0]))
        print("Cval 1: " + str(self.corner_vals[1]))
        print("Cval 2: " + str(self.corner_vals[2]))
        print("Min val: " + str(self.minvalue))
        print("Min pt: " + str(self.minpt))

        return (point,msum)


    def subdivide(self):
        global debugCount
        #Check precision
        if self.get_precision() < max_precision:
            debugCount += 1
            if(debugCount%100 == 0):
                print("REACHED DEEPEST "+str(debugCount))
            return

        self.subregions[0] = Region([self.corners[0], self.corners[1], self.minpt])
        self.subregions[1] = Region([self.corners[1], self.corners[2], self.minpt])
        self.subregions[2] = Region([self.corners[0], self.corners[2], self.minpt])

        for r in self.subregions:
            r.calc_min()

        if self.subregions[0].minvalue == self.subregions[1].minvalue or self.subregions[2].minvalue == self.subregions[1].minvalue or self.subregions[2].minvalue == self.subregions[0].minvalue:
            print (self.minpt)
            return
        for r in self.subregions:
            if r.minvalue > resolved_min:
                print("Minval: " + str(r.minvalue))
                print("Resolved: " + str(resolved_min))
                continue
            heappush(heap,(r.minvalue+(random()*0.0001),r))


def search():
    minx = points[0][0]
    miny = points[0][1]
    maxx = minx
    maxy = miny

    for p in points:
        if p[0] < minx:
            minx = p[0]
        if p[0] > maxx:
            maxx = p[0]
        if p[1] < miny:
            miny = p[1]
        if p[1] > maxy:
            maxy = p[1]

    r1 = Region([[minx,miny],[maxx,miny],[minx,maxy]])
    r1.calc_min()
    print(r1.minvalue)
    heappush(heap,(r1.minvalue,r1))
    r2 = Region([[maxx, maxx], [minx, maxy], [maxx, miny]])
    r2.calc_min()
    print(r2.minvalue)
    heappush(heap, (r2.minvalue, r2))

    while(len(heap)>0):
        r = heappop(heap)[1]
        r.calc_min()
        r.subdivide()


make_points()
search()

print((resolved_min))
print(min_pt)
show()
