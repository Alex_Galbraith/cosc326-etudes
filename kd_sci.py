import scipy.spatial as spatial
import matplotlib.pyplot as plt
import numpy as np
from random import uniform
import smallest_circle as sc
import time


'''Read our points in.'''
def read_telephone_sites(filename):
    telephone_sites = []

    try:
        with open(filename, 'r') as f:
            file_text = f.read()

        telephone_sites = [[float(i) for i in site.split()] for site in file_text.split("\n")[1:]]

        while [] in telephone_sites:
            telephone_sites.remove([])

    except ValueError:
        print("There was a problem in the file!")

    except FileNotFoundError:
        print("Error: '{}' could not be found!".format(filename))

    return telephone_sites

points = read_telephone_sites(input("Please enter a file name.."))
smallest_circle = None
smallest_diameter = float('inf')
enclose = 12

'''Do we have enough points to even bother?'''
if (len(points)<=enclose):
    D = sc.make_circle(points)
    print("Not enough points so I guess the radius is infinite.")
    exit()

'''Plot our solution.'''
def plot(points, solution, circles = None):
    print(D)
    fig, ax = plt.subplots(dpi = 300)
    plt.scatter(points[:,0], points[:,1],  s = 0.1)
    
    plt.axis('equal')
    c = plt.Circle(solution[:2], solution[2], color='red', fill=False)
    ax.add_artist(c)
    
    for cc in circles:
        if cc[2] < 1.2 * solution[2]:
            c = plt.Circle(cc[:2], cc[2], color='m', fill=False, alpha = 0.2, lw = 0.4)
            ax.add_artist(c)
    
    plt.show()



'''Construct our tree.'''
tree = spatial.KDTree(points)
'''Get our groupings.'''
groups = tree.query(points, enclose)[1]
circles = []

'''Loop each grouping, calculate the smallest enclosing circle and save the smallest.'''
for group in groups:
    subset = []
    for p in group:
        subset.append(points[p])
    D = sc.make_circle(subset)
    circles.append(D)
    if(D[2]<smallest_diameter):
        smallest_circle = D
        smallest_diameter = D[2]


plot(points, smallest_circle, circles)
print("Radius: ",smallest_diameter)
    

