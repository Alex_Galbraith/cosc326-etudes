import scipy.spatial as spatial
import matplotlib.pyplot as plt
import numpy as np
from random import uniform
import smallest_circle as sc
import time
import sys



'''Read our points in.'''
def read_telephone_sites(filename):
    telephone_sites = []

    try:
        if filename is "stdin":
            file_text = sys.stdin.read()
        else:
            with open(filename, 'r') as f:
                file_text = f.read()
        fline = file_text.split("\n")[0]

        if (fline.lower() != "telephone sites"):
            print("Header must be 'Telephone Sites'")
        else:
            telephone_sites = [[float(i) for i in site.split()] for site in file_text.split("\n")[1:]]
    
            for site in telephone_sites:
                if (len(site)!=2):
                    telephone_sites.remove(site);
                    print("Foolish mortal! How dare you provide invalid input such as: ",site)

    except ValueError:
        print("There was a problem in the file!")

    except FileNotFoundError:
        print("Error: File '{}' could not be found!".format(filename))

    return telephone_sites

'''Helper to make a random set of points. Not used for etude.'''
def make_points(n, min_range, max_range):
    global points;
    points = np.empty([n,2])
    for i in range(n):
        x = round(uniform(min_range, max_range))
        y = round(uniform(min_range, max_range))
        points[i] = np.array([x, y])


#make_points(1000,0,6000)

input_file_name = "stdin"
if len(sys.argv) is 2:
    input_file_name = sys.argv[1]

points = read_telephone_sites(input_file_name)
points = np.array(points)
smallest_circle = None
smallest_radius = float('inf')
enclose = 12




#Plot our solution.
def plot(points, solution, circles = None):
    return
    fig, ax = plt.subplots(dpi = 300)
    plt.scatter(points[:,0], points[:,1],  s = 0.1)
    
    plt.axis('equal')
    c = plt.Circle(solution[:2], solution[2], color='red', fill=False)
    ax.add_artist(c)
    
    for cc in circles:
        if cc[2] < 1.2 * solution[2]:
            c = plt.Circle(cc[:2], cc[2], color='m', fill=False, alpha = 0.2, lw = 0.4)
            ax.add_artist(c)
    
    plt.show()


#Do we have enough points to even bother?
if (len(points)<=enclose):
    if (len(points) == 0):
        print("No points, exiting.")
    else:
        print("Not enough points so I guess the radius is infinite.")
else:
    #Build the KDTree
    tree = spatial.KDTree(points)
    circles = []
    #Record time
    start = time.time()
    
    #For each point, calculate its nearest enclose -1 neighbours, drawing a circle around them.
    #record the smallest radius of any circle drawn yet. Limit the KDTree search radius to the
    #smallest radius so far. Any search that doesnt find enough points is skipped.
    
    for p in points:
        g = tree.query([p], enclose, distance_upper_bound = smallest_radius*2)[1][0]
        #If not enough points were found, skip this group as it was larger than
        #our smallest radius
        if(g[len(g)-1] == len(points)):
            continue
        #Convert indices to points
        subset = []
        for i in g:
            subset.append(points[i])
        #Calculate the enclosing circle
        D = sc.make_circle(subset)
        circles.append(D)
        #Record smallest
        if (D[2] < smallest_radius):
            smallest_circle = D
            smallest_radius = D[2]
    end = time.time()
    #Plot and print answer
    #plot(points, smallest_circle, circles)
    #print("Took",str(end - start)+"s")
    print(smallest_radius)
        
    
